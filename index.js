var monk = require('monk');
module.exports = {
  config: {
    connectstring : 'localhost/fortest'
  },
  db: null,
  init: function(connstring) {
    this.config.connectstring = connstring;
    this.db = monk(connstring);
  },
  log: function(logstring) {
    if (!this.db) {
      this.db = monk(this.config.connectstring);
    }
    var logs = this.db.get('logs');
    logs.insert({msg: logstring});
  },
  close: function() {
    if (this.db) {
      this.db.close();
    }
  }
}
